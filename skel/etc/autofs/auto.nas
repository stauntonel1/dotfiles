# This is an automounter map and it has the following format
# key [ -mount-options-separated-by-comma ] location
# Details may be found in the autofs(5) manpage

music		-fstype=nfs	mediaschijf3t:/volume1/music
photo		-fstype=nfs	mediaschijf3t:/volume1/photo
video		-fstype=nfs     mediaschijf3t:/volume1/video
data		-fstype=nfs     mediaschijf3t:/volume1/data
